package com.manao.gobelins.manao.data.playlist

import com.manao.gobelins.manao.data.track.TrackEntity
import kaaes.spotify.webapi.android.models.*


class PlaylistEntity {
    var id: String = ""
    var tracks = ArrayList<TrackEntity>()
    var name: String = "Generated Playlist"

    constructor() {}

    constructor(playlist: Recommendations) {
        for (track in playlist.tracks) {
            tracks.add(TrackEntity(track))
        }
    }
}