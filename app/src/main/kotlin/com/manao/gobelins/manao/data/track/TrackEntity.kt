package com.manao.gobelins.manao.data.track

import kaaes.spotify.webapi.android.models.*

class TrackEntity {
    var id: String = ""
    var albumName: String? = null
    var artists: List<ArtistSimple>? = null
    var name: String = ""
    var imageUrl: String? = null

    constructor() {}

    constructor(track: Track) {
        id = track.id
        albumName = track.album.name
        artists = track.artists
        name = track.name
        imageUrl = track.album.images.first().url
    }
}