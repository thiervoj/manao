package com.manao.gobelins.manao.data.artist

import kaaes.spotify.webapi.android.models.Artist

class ArtistEntity {
    var id: String = ""
    var name: String? = null
    var imageUrl: String? = null

    constructor() {}

    constructor(artist: Artist) {
        id = artist.id
        name = artist.name
        imageUrl = artist.images.first().url
    }
}