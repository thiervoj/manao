package com.manao.gobelins.manao.view.playlist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.manao.gobelins.manao.R
import com.manao.gobelins.manao.data.artist.ArtistEntity
import com.manao.gobelins.manao.data.playlist.PlaylistEntity
import com.manao.gobelins.manao.data.track.TrackEntity
import android.support.v7.widget.LinearLayoutManager



class PlaylistDetailActivity : AppCompatActivity() {

    private var viewModel: PlaylistViewModel? = null
    private var tracksAdapter: TracksAdapter? = null

    @BindView(R.id.title)
    lateinit var title: TextView

    @BindView(R.id.tracksRecyclerView)
    lateinit var tracksRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playlist_detail)
        ButterKnife.bind(this)

        val playlistId = intent.getStringExtra(INTENT_PLAYLIST_ID) ?: throw IllegalStateException("field $INTENT_PLAYLIST_ID missing in Intent")

        //TODO get this from intent
        var userId = "0"

        initViewModel(userId, playlistId)

        initTracksAdapter()
    }

    private fun initTracksAdapter() {
        tracksAdapter = TracksAdapter(ArrayList<TrackEntity>())
        tracksRecyclerView.layoutManager = LinearLayoutManager(this)
        tracksRecyclerView.adapter = tracksAdapter
    }

    private fun initViewModel(userId: String, playlistId: String) {
        viewModel = ViewModelProviders.of(this).get(PlaylistViewModel::class.java)

        viewModel?.fetch(userId, playlistId)?.observe(this, Observer<PlaylistEntity> { playlist ->
            run {
                title.text = playlist?.name
                if(playlist?.tracks?.count() != 0) {
                    tracksAdapter?.addItems(playlist!!.tracks.map { it })
                }
            }
        })
    }

    companion object {

        private val INTENT_PLAYLIST_ID = "playlist_id"

        fun newIntent(context: Context, playlist: PlaylistEntity): Intent {
            val intent = Intent(context, PlaylistDetailActivity::class.java)
            intent.putExtra(INTENT_PLAYLIST_ID, playlist.id)
            return intent
        }
    }
}
