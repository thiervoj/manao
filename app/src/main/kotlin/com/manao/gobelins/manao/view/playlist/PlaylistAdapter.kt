package com.manao.gobelins.manao.view.playlist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.manao.gobelins.manao.R
import com.manao.gobelins.manao.data.playlist.PlaylistEntity
import com.manao.gobelins.manao.data.track.TrackEntity


class PlaylistAdapter(private var playlistEntityList: List<PlaylistEntity>?, private var clickListener: View.OnClickListener) : RecyclerView.Adapter<PlaylistAdapter.PlaylistViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistViewHolder {
        return PlaylistViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.playlist_item, parent, false))
    }

    override fun onBindViewHolder(holder: PlaylistViewHolder, position: Int) {
        val playlistEntity = playlistEntityList!![position]

        holder.nameTextView.text = playlistEntity.name
        holder.itemView.setTag(playlistEntity);

        holder.itemView.setOnClickListener(clickListener)
    }

    override fun getItemCount(): Int {
        return playlistEntityList!!.size
    }

    fun addItems(playlistEntityList: List<PlaylistEntity>) {
        this.playlistEntityList = playlistEntityList
        notifyDataSetChanged()
    }

    class PlaylistViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameTextView: TextView

        init {
            nameTextView = view.findViewById(R.id.nameTextView)
        }
    }
}