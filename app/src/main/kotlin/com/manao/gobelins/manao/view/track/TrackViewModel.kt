package com.manao.gobelins.manao.view.track

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.manao.gobelins.manao.data.track.TrackEntity
import com.manao.gobelins.manao.data.track.TrackRepository


class TrackViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = TrackRepository(application)

    fun getTrack(trackId: String): LiveData<TrackEntity> {
        return repository.getTrackById(trackId)
    }
}
