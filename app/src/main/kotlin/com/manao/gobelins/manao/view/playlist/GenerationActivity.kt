package com.manao.gobelins.manao.view.playlist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.SeekBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.manao.gobelins.manao.R

import kotlinx.android.synthetic.main.activity_generation.*
import me.gujun.android.taggroup.TagGroup
import android.widget.Toast
import butterknife.OnClick
import com.manao.gobelins.manao.api.FirebaseWrapper
import com.manao.gobelins.manao.data.playlist.PlaylistEntity
import com.manao.gobelins.manao.view.MainActivity


class GenerationActivity : AppCompatActivity(), SeekBar.OnSeekBarChangeListener {

    private var viewModel: PlaylistViewModel? = null
    private val tagList = listOf("Chill", "Dance", "Electro", "Gospel", "House", "Jazz", "Metal", "Pop", "Punk", "RnB", "Reggae", "Rock", "Techno")
    private var selectedTags = arrayListOf<String>()

    @BindView(R.id.tag_group)
    lateinit var tagGroup: TagGroup

    @BindView(R.id.result)
    lateinit var resultTextView: TextView

    @BindView(R.id.seekBar)
    lateinit var seekBarTempo: SeekBar

    @BindView(R.id.textViewBPM)
    lateinit var textViewBPM: TextView

    private val mTagClickListener = TagGroup.OnTagClickListener { tag ->
        if (selectedTags.contains(tag)) {
            selectedTags.remove(tag)
        } else if (selectedTags.size < 3) {
            selectedTags.add(tag)
        }

        resultTextView.text = "Genres sélectionnés (max 3) : " + selectedTags.toString().trimStart('[').trimEnd(']')
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_generation)
        ButterKnife.bind(this)

        setSupportActionBar(toolbar)

        tagGroup.setTags(tagList)
        tagGroup.setOnTagClickListener(mTagClickListener)

        seekBarTempo.progress = 120
        seekBarTempo.keyProgressIncrement = 10
        seekBarTempo.setOnSeekBarChangeListener(this)
        textViewBPM.text = seekBarTempo.progress.toString()

        Log.d("GENERATION", FirebaseWrapper.currentUserKey)
    }

    @OnClick(R.id.fab)
    fun onCreateClicked(fab: FloatingActionButton) {
        // Instantiate a ViewModel tied to the lifecycle of the fragment
        viewModel = ViewModelProviders.of(this).get(PlaylistViewModel::class.java)

        viewModel?.generate("", "", selectedTags, seekBarTempo.progress)?.observe(this, Observer<PlaylistEntity> { playlist ->
            run {
                // PUSH THE PLAYLIST TO FIREBASE HERE
                // THEN GO TO THE PLAYLIST DETAIL ACTIVITY

                // resultTextView.text = playlist?.tracks?.first()?.name
            }
        })
    }

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        // called when progress is changed
        textViewBPM.text = progress.toString()
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {
        // called when tracking the seekbar is started
    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {
        // called when tracking the seekbar is stopped
    }
}
