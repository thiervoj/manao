package com.manao.gobelins.manao.data.artist

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.manao.gobelins.manao.api.SpotifyApiWrapper
import kaaes.spotify.webapi.android.SpotifyCallback
import kaaes.spotify.webapi.android.SpotifyError
import kaaes.spotify.webapi.android.models.Artist
import retrofit.client.Response


class ArtistRepository internal constructor(application: Application) {

    private val api = SpotifyApiWrapper.instance?.service

    fun getArtistById(artistId: String): LiveData<ArtistEntity> {
        // we need to create our own LiveData instance
        val data = MutableLiveData<ArtistEntity>()

        fetchArtistEntity(artistId, data)

        // return a "promise" which will be resolved when
        // "fetchArtistEntity" is sucessfull
        return data
    }

    private fun fetchArtistEntity(artistId: String, data: MutableLiveData<ArtistEntity>) {
        api?.getArtist(artistId, object : SpotifyCallback<Artist>() {
            override fun failure(spotifyError: SpotifyError) {
                Log.e("fetchArtistEntity", spotifyError.message)
            }

            override fun success(artist: Artist, response: Response) {
                Log.i("fetchArtistEntity", artist.name)

                // make sure the liveData update in the main thread
                data.postValue(ArtistEntity(artist))
            }
        })
    }
}