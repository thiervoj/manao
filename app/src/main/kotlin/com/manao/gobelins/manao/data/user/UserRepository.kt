package com.manao.gobelins.manao.data.user

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.manao.gobelins.manao.api.FirebaseWrapper

class UserRepository internal constructor(application: Application) {

    private val firebaseManager = FirebaseWrapper

    fun getUserByEmail(email: String): LiveData<UserEntity> {
        // we need to create our own LiveData instance
        val data = MutableLiveData<UserEntity>()

        firebaseManager.getUser(email, data)

        // return a "promise" which will be resolved when
        // "fetchUserEntity" is successfull
        return data
    }

    fun createUser(email: String): LiveData<UserEntity> {
        val data = MutableLiveData<UserEntity>()

        firebaseManager.createUser(email, data)

        return data
    }
}