package com.manao.gobelins.manao.view.playlist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.manao.gobelins.manao.R
import com.manao.gobelins.manao.data.track.TrackEntity


class TracksAdapter(private var trackEntityList: List<TrackEntity>?) : RecyclerView.Adapter<TracksAdapter.TrackViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackViewHolder {
        return TrackViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.tracks_item, parent, false))
    }

    override fun onBindViewHolder(holder: TrackViewHolder, position: Int) {
        val trackEntity = trackEntityList!![position]

        holder.nameTextView.text = trackEntity.name
        holder.artistTextView.text = trackEntity.artists?.first()?.name
        holder.albumTextView.text = trackEntity.albumName
    }

    override fun getItemCount(): Int {
        return trackEntityList!!.size
    }

    fun addItems(trackEntityList: List<TrackEntity>) {
        this.trackEntityList = trackEntityList
        notifyDataSetChanged()
    }

    class TrackViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameTextView: TextView
        val artistTextView: TextView
        val albumTextView: TextView

        init {
            nameTextView = view.findViewById(R.id.nameTextView)
            artistTextView = view.findViewById(R.id.artistTextView)
            albumTextView = view.findViewById(R.id.albumTextView)
        }
    }
}