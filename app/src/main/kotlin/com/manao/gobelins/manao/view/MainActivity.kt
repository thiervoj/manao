package com.manao.gobelins.manao.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import butterknife.ButterKnife
import butterknife.OnClick
import com.manao.gobelins.manao.R
import com.manao.gobelins.manao.api.SpotifyApiWrapper
import com.manao.gobelins.manao.data.user.UserEntity
import com.manao.gobelins.manao.view.artist.ArtistDetailFragment
import com.manao.gobelins.manao.view.user.UserViewModel

import com.manao.gobelins.manao.view.playlist.PlaylistListActivity
import com.spotify.sdk.android.authentication.AuthenticationClient
import com.spotify.sdk.android.authentication.AuthenticationRequest
import com.spotify.sdk.android.authentication.AuthenticationResponse
import kaaes.spotify.webapi.android.SpotifyCallback
import kaaes.spotify.webapi.android.SpotifyError
import kaaes.spotify.webapi.android.models.UserPrivate
import retrofit.client.Response


class MainActivity : AppCompatActivity() {

    companion object {
        var TOKEN = ""
    }

    private val CLIENT_ID = "03fd7df9f77045c48a8cb20602f8d469"
    private val REQUEST_CODE = 1001
    private val REDIRECT_URI = "manao://callback"

    private var userViewModel: UserViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ButterKnife.bind(this)
    }

    private fun showArtistDetail() {
        val artistDetailFragment = ArtistDetailFragment()
        val args = Bundle()

        Log.i("MainActivity", "showArtistDetail")

        args.putString(ArtistDetailFragment.ARTIST_ID_KEY, "6pJY5At9SiMpAOBrw9YosS")
        artistDetailFragment.arguments = args
        supportFragmentManager.beginTransaction()
            .add(R.id.container, artistDetailFragment).commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)

        if (requestCode == REQUEST_CODE) {
            val response = AuthenticationClient.getResponse(resultCode, intent)

            if (response.type == AuthenticationResponse.Type.TOKEN) {
                Log.d("AccessToken", response.accessToken)

                SpotifyApiWrapper.instance?.setToken(response.accessToken)

                SpotifyApiWrapper.instance?.service?.getMe(object : SpotifyCallback<UserPrivate>() {
                    override fun failure(spotifyError: SpotifyError) {
                        Log.e("fetchUserEntity", spotifyError.message)
                    }

                    override fun success(user: UserPrivate, response: Response) {
                        Log.i("fetchUserEntity", user.email)

                        start(user.email)
                    }
                })
            }
        }
    }

    private fun start(email: String) {
        // Instantiate a ViewModel tied to the lifecycle of the fragment
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        userViewModel?.getUser(email)?.observe(this, Observer<UserEntity> { user ->
            run {
                if (user != null) {
                    Log.d("FIREBASE", "User already created")
                    val intent = Intent(this, PlaylistListActivity::class.java)

                    startActivity(intent)
                } else {
                    Log.d("FIREBASE", "Has to create user")
                    createUser(email)
                }
            }
        })
    }

    private fun createUser(email: String) {
        userViewModel?.createUser(email)?.observe(this, Observer<UserEntity> { user ->
            run {
                Log.d("FIREBASE", "User created")
                val intent = Intent(this, PlaylistListActivity::class.java)

                startActivity(intent)
            }
        })
    }

    @OnClick(R.id.button_login)
    fun onLoginClick(button: Button) {
        val builder = AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI)

        builder.setScopes(arrayOf( "user-read-email"))
        val request = builder.build()

        AuthenticationClient.openLoginActivity(this@MainActivity, REQUEST_CODE, request)
    }
}
