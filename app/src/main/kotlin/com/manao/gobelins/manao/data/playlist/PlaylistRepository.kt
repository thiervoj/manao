package com.manao.gobelins.manao.data.playlist

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.google.firebase.database.*
import com.manao.gobelins.manao.api.SpotifyApiWrapper
import kaaes.spotify.webapi.android.SpotifyCallback
import kaaes.spotify.webapi.android.SpotifyError
import retrofit.client.Response
import retrofit.http.Query
import retrofit.http.QueryMap
import java.util.*
import kotlin.collections.HashMap
import com.google.firebase.database.GenericTypeIndicator
import com.google.firebase.database.DataSnapshot
import com.manao.gobelins.manao.data.track.TrackEntity
import kaaes.spotify.webapi.android.models.*
import kotlin.collections.ArrayList


class PlaylistRepository internal constructor(application: Application) {

    private val api = SpotifyApiWrapper.instance?.service

    fun generatePlaylist(artistId: String, trackId: String, genres: ArrayList<String>, tempo: Int?): LiveData<PlaylistEntity> {

        val data = MutableLiveData<PlaylistEntity>()
        val params = HashMap<String, Any>()

        if (artistId != "") {
            params.put("seed_artists", artistId)
        }

        if (trackId != "") {
            params.put("seed_tracks", trackId)
        }

        if (genres.size > 0) {
            params.put("seed_genres", genres.toString().toLowerCase().trimStart('[').trimEnd(']'))
        }

        if (tempo != null) {
            params.put("min_tempo", tempo - 5)
            params.put("max_tempo", tempo + 5)
        }

        api?.getRecommendations(Collections.unmodifiableMap(params), object : SpotifyCallback<Recommendations>() {
            override fun failure(spotifyError: SpotifyError) {
                Log.e("fetchPlaylistEntity", spotifyError.message)
            }

            override fun success(recommendations: Recommendations, response: Response) {
                Log.i("fetchPlaylistEntity", recommendations.tracks.toString())

                data.postValue(PlaylistEntity(recommendations))
            }
        })

        return data
    }

    fun fetchPlaylist(userId: String, playlistId: String): LiveData<PlaylistEntity> {
        val data = MutableLiveData<PlaylistEntity>()

        val database = FirebaseDatabase.getInstance()
        val playlistRef = database.getReference("users").child(userId).child("playlists").child(playlistId)


        // Read from the database
        playlistRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.children
                var title: String = ""
                val tracks = ArrayList<TrackEntity>()

                for (child in value) {

                    if (child.key == "name") {
                        title = child.getValue().toString()
                    }

                    if (child.key == "tracks") {
                        for (track in child.children) {
                            val trackObject = TrackEntity()

                            val trackArtists = ArrayList<ArtistSimple>()
                            val firstTrackArtistRef = track.child("artists").children.first()

                            val firstTrackArtist = ArtistSimple()
                            firstTrackArtist.name = firstTrackArtistRef.child("name").value.toString()

                            trackArtists.add(firstTrackArtist)

                            trackObject.name = track.child("name").value.toString()
                            trackObject.albumName = track.child("albumName").value.toString()
                            trackObject.artists = trackArtists

                            tracks.add(trackObject)
                        }
                    }
                }

                val playlist = PlaylistEntity()
                playlist.name = title
                playlist.tracks = tracks

                data.postValue(playlist)
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w(this.javaClass.name, "Failed to read value.", error.toException())
            }
        })


        return data
    }

    fun fetchAllPlaylists(userId: String): LiveData<ArrayList<PlaylistEntity>> {
        val data = MutableLiveData<ArrayList<PlaylistEntity>>()

        val database = FirebaseDatabase.getInstance()
        val playlistRef = database.getReference("users").child(userId).child("playlists")


        // Read from the database
        playlistRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.children
                val playlists = ArrayList<PlaylistEntity>()

                for (child in value) {
                    val playlist = PlaylistEntity()

                    playlist.name = child.child("name").value.toString()
                    playlist.id = child.key

                    playlists.add(playlist)
                }

                data.postValue(playlists)
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w(this.javaClass.name, "Failed to read value.", error.toException())
            }
        })


        return data
    }}