package com.manao.gobelins.manao.api

import kaaes.spotify.webapi.android.SpotifyService
import kaaes.spotify.webapi.android.SpotifyApi

class SpotifyApiWrapper private constructor() {
    private val api: SpotifyApi

    val service: SpotifyService
        get() = api.service

    init {
        if (sInstance != null) {
            throw RuntimeException("Use getInstance()")
        }
        api = SpotifyApi()
    }

    fun setToken(token: String) {
        api.setAccessToken(token)
    }

    companion object {

        @Volatile
        private var sInstance: SpotifyApiWrapper? = null

        val instance: SpotifyApiWrapper?
            get() {
                if (sInstance == null) {
                    synchronized(SpotifyApiWrapper::class.java) {
                        if (sInstance == null) sInstance = SpotifyApiWrapper()
                    }
                }

                return sInstance
            }
    }
}