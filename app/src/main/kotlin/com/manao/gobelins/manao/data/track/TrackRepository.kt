package com.manao.gobelins.manao.data.track

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.manao.gobelins.manao.api.SpotifyApiWrapper
import kaaes.spotify.webapi.android.SpotifyCallback
import kaaes.spotify.webapi.android.SpotifyError
import kaaes.spotify.webapi.android.models.Track
import retrofit.client.Response

class TrackRepository internal constructor(application: Application) {

    private val api = SpotifyApiWrapper.instance?.service

    fun getTrackById(trackId: String): LiveData<TrackEntity> {
        // we need to create our own LiveData instance
        val data = MutableLiveData<TrackEntity>()

        fetchTrackEntity(trackId, data)

        // return a "promise" which will be resolved when
        // "fetchTrackEntity" is successfull
        return data
    }

    private fun fetchTrackEntity(trackId: String, data: MutableLiveData<TrackEntity>) {
        api?.getTrack(trackId, object : SpotifyCallback<Track>() {
            override fun failure(spotifyError: SpotifyError) {
                Log.e("fetchTrackEntity", spotifyError.message)
            }

            override fun success(track: Track, response: Response) {
                Log.i("fetchTrackEntity", track.name)

                // make sure the liveData update in the main thread
                data.postValue(TrackEntity(track))
            }
        })
    }
}