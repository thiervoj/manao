package com.manao.gobelins.manao.data.user

import com.manao.gobelins.manao.data.playlist.PlaylistEntity

class UserEntity {
    var email: String? = null
    var name: String? = null
    var playlists: List<PlaylistEntity>? = null

    constructor() {}

    constructor(email: String, playlists: List<PlaylistEntity>? = null, name: String? = "Tony Montana") {
        this.email = email
        this.name = name
        this.playlists = playlists
    }

    fun toMap(): Map<String, Any?> {
        val result = HashMap<String, Any?>()

        result["email"] = email
        result["name"] = name
        result["playlists"] = playlists

        return result
    }
}