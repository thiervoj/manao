package com.manao.gobelins.manao.api

import android.arch.lifecycle.MutableLiveData
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import android.content.ContentValues.TAG
import android.util.Log
import com.google.firebase.database.DatabaseError
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener
import com.manao.gobelins.manao.data.playlist.PlaylistEntity
import com.manao.gobelins.manao.data.user.UserEntity


/**
 * AWESOME WRAPPER
 * Created by digital on 22/02/2018.
 */
object FirebaseWrapper {
    val dbRef = FirebaseDatabase.getInstance().getReference()
    var currentUserRef: DatabaseReference? = null
    var currentUserKey: String? = "0"

    fun getUser(email: String, data: MutableLiveData<UserEntity>) {
        dbRef.child("users").addListenerForSingleValueEvent(
            object: ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    var i: Long = 0

                    for (child in dataSnapshot.children) {
                        child.ref.addListenerForSingleValueEvent(object: ValueEventListener {
                            override fun onDataChange(snapshotUser: DataSnapshot) {
                                // Get user value
                                val snapshotEmail = snapshotUser.child("email").value

                                if (snapshotEmail == email) {
                                    val user = snapshotUser.getValue(UserEntity::class.java)

                                    // TODO uncomment this when test user is no longer needed
                                    // to show playlists
//                                    currentUserRef = snapshotUser.ref
//                                    currentUserKey = snapshotUser.key

                                    data.postValue(user)
                                } else if (i == dataSnapshot.childrenCount - 1) {
                                    data.postValue(null)
                                }

                                i++
                            }

                            override fun onCancelled(p0: DatabaseError?) {

                            }
                        })
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Log.w(TAG, "getUser:onCancelled", databaseError.toException())
                }
            })
    }

    fun createUser(email: String, data: MutableLiveData<UserEntity>) {
        val key = dbRef.child("users").push().key
        val user = UserEntity(email)
        val updates = HashMap<String, Any>()

        updates["/users/" + key] = user.toMap()

        dbRef.updateChildren(updates).addOnCompleteListener {
            // TODO uncomment this when test user is no longer needed
            // to show playlists
//            currentUserRef = dbRef.child("users/" + key)
//            currentUserKey = key

            data.postValue(user)
        }
    }
}
