package com.manao.gobelins.manao.view.user

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.manao.gobelins.manao.data.user.UserEntity
import com.manao.gobelins.manao.data.user.UserRepository

class UserViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = UserRepository(application)

    fun getUser(email: String): LiveData<UserEntity> {
        return repository.getUserByEmail(email)
    }

    fun createUser(email: String): LiveData<UserEntity> {
        return repository.createUser(email)
    }
}
