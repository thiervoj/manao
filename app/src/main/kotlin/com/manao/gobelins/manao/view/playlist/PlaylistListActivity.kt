package com.manao.gobelins.manao.view.playlist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import butterknife.BindView
import butterknife.ButterKnife
import com.manao.gobelins.manao.R
import com.manao.gobelins.manao.data.playlist.PlaylistEntity
import com.manao.gobelins.manao.data.track.TrackEntity
import com.manao.gobelins.manao.view.MainActivity
import android.content.Intent
import com.manao.gobelins.manao.R.id.fab



class PlaylistListActivity : AppCompatActivity(), View.OnClickListener {

    private var viewModel: PlaylistViewModel? = null
    private var playlistAdapter: PlaylistAdapter? = null

    @BindView(R.id.playlistsRecyclerView)
    lateinit var playlistsRecyclerView: RecyclerView

    @BindView(R.id.fab)
    lateinit var fab: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playlist_list)
        ButterKnife.bind(this)

        val userId = "0"

        initPlaylistAdapter()
        initViewModel(userId)

        fab.setOnClickListener {
            val intent = Intent(this, GenerationActivity::class.java)
            startActivity(intent)
        }
    }


    private fun initPlaylistAdapter() {
        playlistAdapter = PlaylistAdapter(ArrayList<PlaylistEntity>(), this)
        playlistsRecyclerView.layoutManager = LinearLayoutManager(this)
        playlistsRecyclerView.adapter = playlistAdapter
    }

    override fun onClick(v: View?) {
        val playlist = v?.tag as PlaylistEntity
        val intent = PlaylistDetailActivity.newIntent(this, playlist)
        startActivity(intent)
    }

    private fun initViewModel(userId: String) {
        viewModel = ViewModelProviders.of(this).get(PlaylistViewModel::class.java)

        viewModel?.fetchAll(userId)?.observe(this, Observer<ArrayList<PlaylistEntity>> { playlistLists -> run {
            if (playlistLists != null) {
                playlistAdapter?.addItems(playlistLists)
            }
        }})
    }
}
