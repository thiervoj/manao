package com.manao.gobelins.manao.view.artist

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.AndroidViewModel
import com.manao.gobelins.manao.data.artist.ArtistEntity
import com.manao.gobelins.manao.data.artist.ArtistRepository


class ArtistDetailViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = ArtistRepository(application)

    fun getArtist(artistId: String): LiveData<ArtistEntity> {
        return repository.getArtistById(artistId)
    }
}
