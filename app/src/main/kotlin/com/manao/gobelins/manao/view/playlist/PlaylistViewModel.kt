package com.manao.gobelins.manao.view.playlist

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.AndroidViewModel
import android.util.Log
import com.manao.gobelins.manao.data.playlist.PlaylistEntity
import com.manao.gobelins.manao.data.playlist.PlaylistRepository


class PlaylistViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = PlaylistRepository(application)

    fun generate(artistId: String, trackId: String, genres: ArrayList<String>, tempo: Int?): LiveData<PlaylistEntity> {
        return repository.generatePlaylist(artistId, trackId, genres, tempo)
    }

    fun fetch(userId: String, playlistId: String): LiveData<PlaylistEntity> {
        return repository.fetchPlaylist(userId, playlistId)
    }

    fun fetchAll(userId: String): LiveData<ArrayList<PlaylistEntity>> {
        return repository.fetchAllPlaylists(userId)
    }
}
