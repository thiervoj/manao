package com.manao.gobelins.manao.view.artist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.manao.gobelins.manao.R
import com.manao.gobelins.manao.data.artist.ArtistEntity


/**
 * A simple [Fragment] subclass.
 */
class ArtistDetailFragment : Fragment() {
    companion object {
        const val ARTIST_ID_KEY = "ARTIST_ID_KEY"
    }

    private var viewModel: ArtistDetailViewModel? = null

    @BindView(R.id.artist_detail_name)
    lateinit var nameTextView: TextView

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_artist_detail, container, false)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ButterKnife.bind(this, view)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val artistId = arguments.getString(ARTIST_ID_KEY)

        // Instantiate a ViewModel tied to the lifecycle of the fragment
        viewModel = ViewModelProviders.of(this).get(ArtistDetailViewModel::class.java)

        viewModel?.getArtist(artistId)?.observe(this, Observer<ArtistEntity> { artist ->
            run {
                nameTextView.text = artist?.name
            }
        })
    }
}
