# Manao - Spotify Playlist Generator

Manao is an Android app offering you to generate playlists from various parameters.
It uses your Spotify account to work.

## Functionalities

Here is a list of the functionalities developed :

- Spotify connection
- Dashboard with a list of user's playlists
- List of tracks in a single playlist
- Playlist generation
- Firebase synchronisation

### Connection

The connection uses the Spotify Auth SDK to connect to our Developer App and call the API.

When you log in for the first time, your email address is registered in our Firebase database.

If your email already exists in the database, we just fetch your data (list of *Playlists*, *Tracks*...) from Firebase.

### Dashboard

Once connected, you are redirected to your *Dashboard*, where he can see a list of your previously generated *Playlists*.

**In the app, we are using a hard-coded user id to fetch the *Playlists* from Firebase, as we did'nt have enough time to develop the part that saves the generated Playlist in Firebase. (See the `Generation` part for details).**

### Playlist details

By clicking on a *Playlist* in the *Dashboard*, you will see a list of *Tracks* that are contained in this *Playlist*.

### Generation

You can access the *Generation* view by clicking the `+` button in the bottom-right corner of the screen.

Here, you can choose up to 3 music genres, and select an average tempo for the *Tracks* that will be generated in the *Playlist*.

**As said in the `Dashboard` part, the *Generation* works but the Firebase implementation is not done. This means that once you click the button to generate your *Playlist*, it is created and logged in the `LogCat`, but it is not saved into Firebase so you will not see it in your Dashboard.**

### Firebase synchronisation

Firebase collects all the data related to every user :

- users
	- email
	- playlists
		- name
		- creationDate
		- tracks
			- albumName
			- imageUrl
			- name
			- artists
				- name
				- imageUrl